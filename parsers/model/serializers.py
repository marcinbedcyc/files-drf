from rest_framework import serializers
from .models import SimpleFileModel


class SimpleFileSerializer(serializers.ModelSerializer):
    author_first = serializers.CharField(required=False)
    filename = serializers.CharField(required=False)

    class Meta:
        model = SimpleFileModel
        fields = ("id", "filename", "author", "author_first", "created_date", "modified_date")
