from django.urls import path, include
from rest_framework import routers

from .views import SimpleFileViewSet

router = routers.DefaultRouter()
router. register("simple-file", SimpleFileViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
