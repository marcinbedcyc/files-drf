from rest_framework import viewsets
from .models import SimpleFileModel
from .serializers import SimpleFileSerializer
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import ParseError
from rest_framework.decorators import action
from django.http import HttpResponse


class SimpleFileViewSet(viewsets.ModelViewSet):
    queryset = SimpleFileModel.objects.all()
    serializer_class = SimpleFileSerializer
    parser_classes = (FormParser, MultiPartParser)
    content_type = 'text/xml'

    def create(self, request, *args, **kwargs):
        print("CREATE")
        file_serializer = SimpleFileSerializer(data=request.data)

        if 'binary_file' not in request.data:
            raise ParseError("Empty content")

        if file_serializer.is_valid():
            model = SimpleFileModel(
                filename=file_serializer.validated_data['filename'] if
                'filename' in file_serializer.validated_data.keys() else (
                    request.data['binary_file'].name
                ),
                author=file_serializer.validated_data['author'],
                author_first=file_serializer.validated_data['author'],
                binary_file=request.data['binary_file'].read()
            )
            model.save()
            return Response(SimpleFileSerializer(model).data, status=status.HTTP_201_CREATED)

        return Response(file_serializer.errors, status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        print("UPDATE")
        file_serializer = SimpleFileSerializer(data=request.data)
        instance = self.get_object()

        if 'binary_file' not in request.data:
            raise ParseError("Empty content")

        if file_serializer.is_valid():
            instance.filename = file_serializer.validated_data['filename'] if (
                'filename' in file_serializer.validated_data.keys()
            ) else request.data['binary_file'].name
            instance.author = file_serializer.validated_data['author']
            instance.binary_file = request.data['binary_file'].read()
            instance.save()
            return Response(SimpleFileSerializer(instance).data, status=status.HTTP_200_OK)

        # return super().update(request, *args, **kwargs)
        return Response(file_serializer.errors, status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, *args, **kwargs):
        print("PARTIAL UPDATE")
        file_serializer = SimpleFileSerializer(data=request.data)
        instance = self.get_object()

        if file_serializer.is_valid():
            if 'binary_file' in request.data or 'filename' in file_serializer.validated_data.keys():
                instance.filename = file_serializer.validated_data['filename'] if (
                    'filename' in file_serializer.validated_data.keys()
                ) else request.data['binary_file'].name

            if 'author' in file_serializer.validated_data.keys():
                instance.author = file_serializer.validated_data['author']

            if 'binary_file' in request.data:
                instance.binary_file = request.data['binary_file'].read()

            instance.save()
            return Response(SimpleFileSerializer(instance).data, status=status.HTTP_200_OK)

        return Response(file_serializer.errors, status.HTTP_400_BAD_REQUEST)
        # return super().partial_update(request, *args, **kwargs)

    @action(methods=['get'], detail=True)
    def download(self, *args, **kwargs):
        instance = self.get_object()

        response = HttpResponse(bytes(instance.binary_file), content_type=f'{self.content_type}')
        response['Content-Disposition'] = f'attachment; filename="{instance.filename}"'

        return response
