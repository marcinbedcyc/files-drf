from django.db import models


class SimpleFileModel(models.Model):
    binary_file = models.BinaryField()
    filename = models.TextField()
    author = models.TextField()
    author_first = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
